arachnocheck
============

Submit results to CDash showing whether a list of websites are up or down

# Prerequisites

* ``sh``

* ``curl``

* CMake

# Usage

1. Edit `urls.txt` to include the list of sites you want to check, one per line.

2. Create a build directory somewhere, and run

    cmake /path/to/source/dir

3. Run ``ctest`` and get a report of which of your sites are up!

# Notes

Arachnocheck works by examining the headers delivered by ``curl`` for a
particular URL.  A response code of ``200`` is taken to mean the site is alive.

Currently, arachnocheck doesn't support HTTP 301 redirects - these will count as
test failures, so you should put the "final" URL in `urls.txt`.  This may change
in a future version, so that redirects leading to a ``200``-producing URL will
also count as passing tests.
