#!/bin/sh

url="$1"

if [ -z ${url} ]; then
    echo "usage: arachnocheck.sh <url>" 1>&2
    exit 2
fi

response=`curl -s -k -I ${url} | head -n 1 | cut -d " " -f 2`
if [ -z "${response}" ]; then
    response="could not connect"
fi

echo ${response}

if [ "${response}" != "200" ]; then
    exit 1
fi
